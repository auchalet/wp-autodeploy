#!/bin/bash


#Dossier www dans le /home
if [ ! -d "/home/$USER/websites" ];then
    echo "Création dossier";
    mkdir -f /home/$USER/websites;
fi


export WEBSITES="/home/$USER/websites"


echo "============================================"
echo "Nom de votre site ? (= Dirname)";
echo "============================================"   
read -e wp_name

# Si le site n'existe pas, on fait toute l'install
if [ ! -d "$WEBSITES/$wp_name" ];then

	echo "============================================"
	echo "Installation des pré-requis"
	echo "============================================"

	#Pré-requis
	sudo apt-get update
	sudo apt-get install apache2 php5 mysql-server libapache2-mod-php5 php5-mysql

	echo "============================================"
	echo "Téléchargement de WP + Installation dans /home/$USER/websites"
	echo "============================================"


	cd /home/$USER

	#Téléchargement WP
	if [ ! -f "/home/$USER/latest.tar.gz" ];then
	    echo "============================================"
	    echo "Téléchargement de la dernière version de WP :";
	    echo "============================================"    
	    curl -O https://wordpress.org/latest.tar.gz 
	    
	else
	    echo "Archive déjà téléchargée"
	fi







	#Unzip WP
	if [ ! -d "/home/$USER/wordpress" ];then
	    tar -zxvf latest.tar.gz
	    mv wordpress $wp_name
	else
	    rm -rf /home/$USER/wordpress
	    tar -zxvf latest.tar.gz
	    mv wordpress $wp_name
	fi



	#Déplacer le dossier dans /home
	mv $wp_name $WEBSITES

	#Lien symbolique vers /var/www
	echo "============================================"
	echo "Création d'un lien vers /var/www + Droits"
	echo "============================================"

	sudo ln -s $WEBSITES/$wp_name /var/www




	cd $WEBSITES/$wp_name


	echo "============================================"
	echo "Configuration DB";
	echo "============================================"   

	echo "Activation du service MySQL"
	sudo service mysql start


	echo "-> DB Name"
	read dbname

	echo "-> DB User"
	read dbuser

	echo "-> DB Password"
	read -s dbpswd

	echo "============================================"
	echo "Création de la DB";
	echo "============================================"  

	echo "-> Connexion MySQL"
	mysql -u root -p -e "
	    CREATE USER '$dbuser'@'localhost'; 
	    CREATE DATABASE $dbname;
	    SET password FOR '$dbuser'@'localhost' = password('$dbpswd');
	    GRANT ALL ON $dbname.* TO '$dbuser'@'localhost';
	    SHOW databases;
	"

	echo "-> Configuration du wp-config.php"

	#create wp config
	cp wp-config-sample.php wp-config.php

	#set database details with perl find and replace
	perl -pi -e "s/database_name_here/$dbname/g" wp-config.php
	perl -pi -e "s/username_here/$dbuser/g" wp-config.php
	perl -pi -e "s/password_here/$dbpswd/g" wp-config.php

	#create uploads folder and set permissions
	mkdir wp-content/uploads
	chmod 775 wp-content/uploads


	#Suppression de l'archive
	#rm -rf /home/$USER/latest.tar.gz
else
	#Si le site existe on se déplace dans son dossier pour l'installer
	cd $WEBSITES/$wp_name

fi



echo "============================================"
echo "Installation via WP-CLI";
echo "============================================"  

#Test pour savoir si WP-Cli est déjà installé -- Ne détecte pas le dossier...
if [ ! -d "/usr/local/bin/wp" ];then
    curl -O https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar
    chmod +x wp-cli.phar
    sudo mkdir /usr/local/bin/wp
    sudo mv wp-cli.phar /usr/local/bin/wp

    echo "WP-CLI installé, utilisez la commande 'wp'"
else
    echo "WP-CLI est déjà installé"
fi

# Config install du site
echo "-> Configuration du site"

echo "Adresse du site"
read -e addr

echo "Nom du site (Title)"
read -e title

echo "Admin - Login"
read -e admin

echo "Admin - Password"
read -s adminpwd

echo "Admin - Email"
read -e adminmail

# Install et Update
echo "-> Installation de Wordpress"
install=$(wp core install --url="$addr" --title="$title" --admin_user="$admin" --admin_password="$adminpwd" --admin_email="$adminmail")


echo "-> Mise à jour de Wordpress"
wp core update

echo $install


# Gestion des thèmes
echo "Thèmes"
echo "-> Désinstallation des thèmes pré-installés"

wp theme delete twentyfifteen
wp theme delete fourteen

echo "-> Installation d'un thème"
echo "Installer ou Rechercher ou Quitter ? [I/R/Q]"
read -e temp

OPTIONS="I R Q"

select temp in $OPTIONS
do
    echo $temp

    if [ $temp == "R" ];then

         echo "Rechercher un thème"
         read -e theme
         wp theme search $theme
    elif [ $temp == "I" ];then

         echo "Installer un thème"
         read -e theme
         wp theme install $theme --activate

    elif [ $temp == "Q" ];then
	exit;

    fi

    echo "Installer ou Rechercher ou Quitter ? [I/R/Q]"
    read -e temp

done





# Droits d'accès
sudo addgroup $USER www-data
sudo chown -R $USER:www-data $WEBSITES/$wp_name
sudo chmod 775 -R $WEBSITES/$wp_name

echo "Install terminée"
exit;



