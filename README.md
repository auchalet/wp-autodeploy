
# Script d'installation automatique de Wordpress

Clôner le dépôt :
```
git clone https://gitlab.com/auchalet/wp-autodeploy.git
```

Lancer le script et laissez-vous guider :
```
cd wp-autodeploy
chmod +x wp_install_auto.sh
./wp_install_auto.sh
```

